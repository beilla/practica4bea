$(document).ready(function(){
    //$("#idUser").value=localStorage.getItem('token');
    // document.getElementById('idUser').innerText =localStorage.getItem('token');
});


function nuevoTipo()
{
    $.ajax({
        url:"/tipos",
        type:"POST",
        data: JSON.stringify({nombre: $("#nombre").val()}),
        contentType:"application/json; charset=utf-8",
        dataType:"json",
        success: function(data) {
            if (data.error) {
                $("#nuevoError").css('display', 'block');
                $("#nuevoOK").css('display', 'none');
            } else {
                $("#nuevoError").css('display', 'none');
                $("#nuevoOK").css('display', 'block');
            }
        }
    });
}

function borrar($id)
{
    $.ajax(
        {
            url:"/inmuebles/" + $id, 
            type: "DELETE",
            data: JSON.stringify({}),
            contentType:"aplication/json; charset=utf-8",
            dataType: "json",
            beforeSend: (xhr) => {
                xhr.setRequestHeader('Authorization', 'Bearer ' + localStorage.getItem('token'));
            },
            success: () => {
                document.location.href="/inmuebles";
            }
        }
    );
}

function login()
{
    // console.log(document.getElementById('login').value);
    $.ajax({
        url:"/usuarios/login",
        type:"POST",
        data: JSON.stringify({login: document.getElementById('login').value, password: document.getElementById('password').value}),
        contentType:"application/json; charset=utf-8",
        dataType:"json",
        cache: false,
        beforeSend: (xhr) => {
            xhr.setRequestHeader('Authorization', 'Bearer ' + localStorage.getItem('token'));
        },
        success: function(data) {
            // console.log(data);
            if (data.error) {
                $("#loginError").css('display', 'block');
            } else {
                $("#loginError").css('display', 'none');
                //alert("Token: " + data.token);
                window.location = data.newLocation;
                localStorage.setItem("token", data.token);
                cargar('/inicio');
            }
        }
    });
}

function nuevoInmueble(metodo)
{
    var formData = new FormData(document.getElementById('formulario'));
    //console.log(formData);
    $.ajax({
        url:"/inmuebles",
        type:"POST",
        data: formData,
        contentType:false,
        cache:false,
        processData: false,
        beforeSend: (xhr) => {
            xhr.setRequestHeader('Authorization', 'Bearer ' + localStorage.getItem('token'));
        },
        success: function(data) {
            console.log(data);
            if (data.error) {
                $("#nuevoInmuebleError").css('display', 'block');
            } else {
                $("#nuevoInmuebleError").css('display', 'none');
                //alert("Token: " + data.token);
                window.location = data.newLocation;
                // localStorage.setItem("token", data.token);
                //cargar('/inicio');
            }
        }
    });
}

function protegido() {
    $.ajax({
        url:"/protegido",
        type:"GET",
        beforeSend: function(xhr) {
            xhr.setRequestHeader('Authorization', 'Bearer '+localStorage.getItem('token'));
        },
        success: function(data) {
            $('#contenido').html(data);
        }
    });
}