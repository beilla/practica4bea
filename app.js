const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const passport = require('passport');
const fileUpload=require('express-fileupload');
const jwt = require('jsonwebtoken');

const inmuebles = require(__dirname +'/routes/inmuebles');
const tipos =require(__dirname +'/routes/tipos');
const index = require(__dirname +'/routes/index');
const usuarios = require(__dirname +'/routes/usuarios');
const Usuario = require(__dirname +'/models/usuario');

const secreto = "secretoDAW";

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/inmuebles');


let app=express();
app.use(passport.initialize());
app.set('view engine', 'ejs');

app.use(fileUpload()); //también hace de body parser procesa los datos que llegan del formulario
app.use(bodyParser.json());//cada petición que llegue al servidor desde el cliente lo va a parsear a json con este middleware
app.use(bodyParser.urlencoded({extended:false}));

app.use(express.static(__dirname + '/public'));//todas las peticiones que sean estáticas las sirves en public


//Decodifica el token
let validarToken =(token)=> {
    try {
        let resultado = jwt.verify(token, secreto);
        return resultado;
    } catch (e) {
        //console.log(e);
    }
}

/* app.use((req, res, next) => {
    let token = req.headers['authorization'];
    if(token){
        token=token.substring(7);
        console.log(token);
        let tokenOk = validarToken(token);
        Usuario.findOne({ _id: tokenOk.id }).then(result => {
            res.locals.nombreUser = result.nombre;
            console.log(result);
            next();
        }).catch(error => {
            res.locals.nombreUser = undefined;
            next();
        })
    }else{
        next();
    }
    
}); */

app.use('/inmuebles', inmuebles);
app.use('/tipos', tipos);
app.use('/usuarios', usuarios);
app.use('/', index);

app.use((req, res, next) => {
    res.status(404);
    res.render('404', { url: req.url });
});

module.exports.app = app;

