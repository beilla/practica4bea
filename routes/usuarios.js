const express = require('express');
const fileUpload = require('express-fileupload');
let fs = require('fs');
const mongoose = require('mongoose');
const md5 = require('md5');
const jwt = require('jsonwebtoken');

const Usuario = require(__dirname +'/../models/usuario');
let Inmueble = require(__dirname +'/../models/inmueble.js');
let Tipo = require(__dirname +'/../models/tipo');

let router = express.Router();

//---GENERO EL TOKEN-------------------
const secreto = "secretoDAW";
let generarToken = id => {
    return jwt.sign({ id: id }, secreto, { expiresIn: "24 hours" });
}

/*********** ENRUTADORES DE LAS PETICIONES ***************/

//-----------INSERTAR USUARIO-------------------------
router.post('/registro', (req, res) => {

    let nuevoUsuario = new Usuario({
        nombre: req.body.nombre1,
        login: req.body.login1,
        password: md5(req.body.password1)
    });
    nuevoUsuario.save().then(resultado => {
        res.render("login", {error:undefined});
    }).catch(errorMsg => {
        // console.log("error:",errorMsg);
        res.render(__dirname + '/../views/registro', { error: true, mensajeError: errorMsg });
    });
});

//-----------VALIDAR USUARIO-------------------------
router.post('/login', (req, res) => {
    //console.log(req.body);
    Usuario.findOne({ login: req.body.login, password: md5(req.body.password) })
        .then(resultado => {
            if (resultado)
                res.send({ ok: true, token: generarToken(resultado._id), newLocation:'/' });
            else
                res.send({ ok: false, mensajeError: "Datos incorrectos" });
        }).catch(error => {
            res.send({ ok: false, mensajeError: "Usuario no encontrado" });
        });
});

module.exports = router;