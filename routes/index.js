const express = require('express');
let router = express.Router();
let Tipo = require(__dirname +'/../models/tipo');

/**************RUTAS PÚBLICAS ***************************/

//--------------PRINCIPAL---------------------
router.get('/', (req, res) => {
    res.render(__dirname + '/../views/index');
});

//--------------NUEVO INMUEBLE---------------------
router.get('/nuevo_inmueble', (req, res) => {
    Tipo.find().then(resultado => {
        // console.log(resultado);
        res.render(__dirname + '/../views/nuevo_inmueble', { error: undefined, tipos: resultado });
    }).catch(error => {
        res.render(__dirname + '/../views/nuevo_inmueble', { error: undefined, tipos: [] });
    });
});

//--------------NUEVO TIPO---------------------
router.get('/nuevo_tipo', (req, res) => {
    res.render(__dirname + '/../views/nuevo_tipo',{ error: undefined });
});

//-----------mostrar LOGIN-------------------------
router.get("/login", (req, res) => {
    res.render(__dirname + '/../views/login', {error:undefined});
});

//-----------mostrar REGISTRO-------------------------
router.get("/registro", (req, res) => {
    res.render(__dirname + '/../views/registro', {error:undefined});
});

//-----------ACCESO NO PERMITIDO----------------
router.get('/prohibido', (req, res) => {
    res.render(__dirname + '/../views/prohibido');
});

router.get('/prohibido/ajax', (req, res) => {
    res.send({ newLocation: '/prohibido'});
})

module.exports=router;